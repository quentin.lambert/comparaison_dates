# -*- coding: utf-8 -*-
"""
Created on Wed Oct 26 15:00:11 2022

@author: quentin.lambert
"""

#importation des modules nécessaires
import os.path, os, psycopg2, csv, glob
from qgis.core import QgsApplication, QgsProject, QgsLayoutExporter

# Supply path to qgis install location
QgsApplication.setPrefixPath('C:/Program Files/QGIS 3.24.1/bin/qgis-bin.exe', True)

# Create a reference to the QgsApplication. Setting the
# second argument to False disables the GUI.
qgs = QgsApplication([], False)

# Load providers
qgs.initQgis()

# Write your code here to load some layers, use processing
# algorithms, etc.

# Finally, exitQgis() is called to remove the
# provider and layer registries from memory
#qgis.exitQgis()

def export_atlas(qgs_project_path, output_folder):

     # Open existing project
     project = QgsProject.instance()
     project.read(qgs_project_path)

     # liste layout et export 
     myLayouts = QgsProject.instance().layoutManager()
     for layout in myLayouts.printLayouts():
          print(layout.name()) 
          name=layout.name()
          name=name.lower()
          if name[:5]=='atlas':
               layout = project.layoutManager().layoutByName(layout.name())
               output_folder_carte= output_folder+'carte-'+name[6:]+'.pdf'
               QgsLayoutExporter(layout).exportToPdf(output_folder_carte,QgsLayoutExporter.PdfExportSettings())
  
def main():
     # Start a QGIS application without GUI
     qgs = QgsApplication([], False)
     qgs.initQgis()

     #Chemin des projets qgs, chemin du répertoire d'export et nom du composeur      
     project_path = 'D:/2_projets_qgs/'
     output_folder = 'D:/0_docs/pdf_sans_ouvrir_qgis/pdf/'
      
     export_atlas(project_path, output_folder)

     # Close the QGIS application
     qgs.exitQgis()

if __name__ == "__main__":
    main()

#création d'une liste contenant tout les fichiers qgis du dossier 1_Projets
fichiers_qgs = []
# r=root, d=dossiers, f = fichiers
for r, d, f in os.walk('D:/2_projets_qgs/'):
    #pour chaque fichier
    for fichier in f:
        #si la chaîne .qgs apparait dans le nom du fichier
        if '.qgs' in fichier:
            #on ajoute à la liste le nom du fichier
            fichiers_qgs.append(os.path.join(r, fichier))

def init_tableau():
    try :
        conn = psycopg2.connect(
            user = "postgres",
            password = "Limoges*87",
            host = "localhost",
            port = "5432",
            database = "geobase_dreal"
        )
        # Ouvrir un curseur pour envoyer une commande SQL
        cur = conn.cursor()
        
        # On execute la commande SQL
        sql = "SELECT schemaname||'.'||tablename nom_jdd  FROM admingeo.catalogue_patrimoine  WHERE cast(date_last_modif as date)=(current_date-1)"
        cur.execute(sql)
        
        # On récupère les données ligne par ligne
        res = cur.fetchall()
        
        # on définit le chemin de stockage de la table
        path = "D:\\test\\" + "catalogue_patrimoine_drealna_pg_tt" + ".csv"

        # On ouvre le fichier csv en mode écriture[https://pythonforge.com/module-csv-lire-ecrire/]
        fichier = open(path,'w')
            
        # On créé un objet csv à partir du fichier
        obj = csv.writer(fichier)
        
        # On boucle pour créer les lignes du tableau et les placer dans un fichier txt avec separateur ';'
        for element in res:
            obj.writerow(element)
            #pour chaque fichiers dans la liste des qgis
            for qgs in fichiers_qgs:
                #on remplace les backslash par des slash
                qgs = qgs.replace(os.sep, '/')
                #on fait la distinction entre chemin du repertoire et nom du fichier qgs
                chemin_qgs, tail = os.path.split(qgs)
                #la chaîne à chercher est le nom du jdd de la ligne selectionnée
                #nom_jdd = element[0]
                #le fichier à ouvrir est celui correspondant à la ligne selectionnée
                fichier_ouv = open(qgs,"r")
                #pour chaque ligne du code source du fichier
                for ligne in fichier_ouv:
                    if "<datasource>dbname='geobase_dreal" in ligne:
                        position = ligne.find('table=')
                        nom = ligne[position:]
                        name=nom.replace('"','')
                        #si la chaîne cherchée se trouve dans le fichier
                        if name == element[0]:
                            #Création du fichier "Liste_projet.txt" listant les chemins vers les projets *.qgs et nécessaire pour faire tourner l'extension pdf_sans_ouvrir_qgis

                            # Pointer vers le répertoire de  stockage des projets qgis
                            project_path = r"D:\0_docs\pdf_sans_ouvrir_qgis"

                            # Création du fichier Liste_projet.txt
                            with open ("D:\0_docs\pdf_sans_ouvrir_qgis\Liste_projet.txt","w") as liste :
                                # la méthode glob.glob permet de lister les fichiers avec l’extension *.qgs présent dans les répertoires et sous-répertoires
                                for elts in glob.glob(project_path +"/**/*.qgs"):            
                                    liste.write(elts + "\n") 
                                      
                            exec(r'D:\0_docs\pdf_sans_ouvrir_qgis\n-export_pdf.py')
                            liste.close()
                            fichier_ouv.close()
                            #on casse la boucle
                            break
    

        #fermeture du catalogue
        fichier.close()
  
        #fermeture de la connexion à la base de données
        cur.close()
        conn.close()

    except (Exception, psycopg2.Error) as error :
        print ("Erreur lors de l'insertion.", error)

init_tableau()